import os
import datetime
from collections import OrderedDict
from utils.files import fileSize
from utils.yaml import writeDict
from utils.strings import StringFormatter

def cmdOrLink(cmd, sharedFile, output):
  """
  Returns the given :cmd: shell command if the shared file :sharedFile: does not exists, otherwise the command to link the file to the :output:.
  Returns also a boolean for forcing the command execution in debug mode. 
  """
  if sharedFile and os.path.exists(sharedFile):
    return (
      f" ln -s {sharedFile} {output} ",
      True)
  return (cmd, False)
    
def exshell(cmd='', isSubCmd=False, force=False, **kwargs):
  """
  Formats the given command.
  Prepands the system's conda activation if specified in configuration.
  Prints the target's log if specified in the configuration.
  Executes the command if debug mode not set in configuration.
  """

  """ Separate workflow commands from stats """
  cmdLog  = cmdAddEnv(cmd)

  """ Write target's log """
  timeStart = datetime.datetime.now()
  writeCmdLog(cmdLog, timeStart=timeStart, **kwargs)
 
  if pypette.stats:
    cmdFull = cmdAddStats(cmdLog, **kwargs)
  else:
    cmdFull = cmdLog

  """ Protect Missing Keywords """
  cmdFull = StringFormatter(cmdLog).formatPartialMap(keepMissingKeys=True, protectKw=True, **kwargs)

  """ Debug mode """
  if config__valueOf('debug') and not force:
    touchOutputs(**kwargs)
  else:
    shell(cmdFull, **kwargs) 
  timeStop = datetime.datetime.now()

  """ Update target's log """
  writeCmdLog(cmdLog, timeStart=timeStart, timeStop=timeStop, **kwargs)

def touchOutputs(output, rule, **kargs):
  """
  Touches the given rule's outputs. Deals with regular files and directories.
  """
  ruleWfOutputs = list(filter(lambda x: x.name==rule, workflow.rules))[0].output

  outdirsWf  = list(filter(lambda x: x.is_directory, ruleWfOutputs))
  outdirs  = []
  outfiles = []
  for outdirWf in outdirsWf:
    outdirs += list(filter(lambda x: outdirWf.match(x), output))
  outfiles = list(filter(lambda x: x not in outdirs, output))

  list(map(lambda x: os.makedirs(x, exist_ok=True), outdirs))
  touch(outfiles)

# ------------
# Environment
# ------------
def cmdAddEnv(cmd, **kwargs):
  return f"""
    {cmdEnv()}
    {cmd}
  """

def cmdEnv():
  return f"""
    condactivate pypette-{pypette.pipeName} || echo "Issue activating conda env"
  """

# ------------
# Stats Logs
# ------------
def cmdAddStats(cmd='', **kwargs):
  return f"""
    {cmdStats(**kwargs)}
    {cmd}
  """

def cmdStats(**kwargs):
  #
  # CSV formatted stats Command
  #
  pidFile   = f"{targetName(**kwargs)}.pid"
  statsFile = f"{targetName(**kwargs)}.stats"
  interval  = '1'
  ret = [
    "echo $BASHPID > " + pidFile,
    "(S_TIME_FORMAT=ISO pidstat -u -r -d -h -p $(cat " + pidFile + ") " + interval \
     + " | tail -n +3 | sed -e '1s/^#//' -e '/^#\|^$/d' -e 's/^[ ]*//' -re 's/[[:blank:]]+/,/g'" \
     + " > " + statsFile + " &)"
  ]
  return os.linesep.join(ret)

# ------------
# Write Logs
# ------------
def writeCmdLog(cmd=None, logFile=None, **kwargs):
  if not config__valueOf('logCommands'):
    return
  logFile = f"{targetName(**kwargs)}.info.yaml"

  # TODO: Auto attribute missing fields in output. 
  # ?In which case is it needed?
  if 'sample_name' not in kwargs.keys():
    kwargs['sample_name'] = 'all'

  writeDict(logCmdDict(cmd, **kwargs), logFile)

def logCmdDict(cmds, **kwargs):
  """
  Sets the commands's OrderedDict for yaml export.
  """
  return OrderedDict({
    'rule'    : kwargs['rule'],
    'inputs'  : logFmtFilesYaml(kwargs['input']),
    'outputs' : logFmtFilesYaml(kwargs['output']),
    'commands': cmds.split(os.linesep),
    'commands': logFmtCmdYaml(cmds, **kwargs),
    'time'    : logCmdTimeDict(**kwargs),
    'modality': 'debug' if config__valueOf('debug') else 'pipeline' 
  })

def logCmdTimeDict(**kwargs):
  return OrderedDict({
    'timeStart'  : str(kwargs['timeStart'])  if 'timeStart'  in kwargs.keys() else '---',
    'timeStop'   : str(kwargs['timeStop'] )  if 'timeStop'   in kwargs.keys() else '---',
    'elapsed'    : str(kwargs['timeStop'] - kwargs['timeStart']) if 'timeStart' in kwargs.keys() and 'timeStop' in kwargs.keys() else '---'
   })

def logFmtFilesYaml(files):
  """
  Format a list of :files: for yaml export.
  """
  return [
    f"{str(file)} ({fileSize(str(file))})"
    for file in files
  ]

def logFmtCmdYaml(cmds="", **kwargs):
  """
  Formats the given string of :cmds: for yaml export.
  """
  return [ strRmSpaces(cmd) for cmd in cmds.split(os.linesep) if cmd.strip()]

def strRmSpaces(words):
  """
  Removes blank spaces from the given string of :words:.
  """
  return ' '.join([ word.strip() for word in words.split(' ') if word.strip() ])

def targetName(**kwargs):
  """
  Gives a default basename for target's log.
  """
  if kwargs['output']:
    ret = f"{kwargs['output'][0]}"
  else:
    ret = kwargs['rule']
  return ret

