## v0.6.1 - *08/10/2020*

## v0.6.0 - *08/10/2020*
* Latest version of pypette can be deployed automatically in production.
* Added coverage module for Dna pipelines.
* Info file for each target now indicates elapsed time.

## v0.5.0 - *29/09/2020*
* RNASeq: Added bam coverage feature.

## v0.4.7 - *31/07/2020*

## v0.4.6 - *29/07/2020*

## v0.4.5 - *29/07/2020*

## v0.4.4 - *29/07/2020*

## v0.4.3 - *29/07/2020*

## v0.4.2 - *29/07/2020*
* Updated release version name; Added a reset command for releases.
* Added --stats option to log resource stats for each target.

## 0.4.1 - *24/07/2020*

## 0.4.0 - *20/07/2020*
* Project is now optional, by default it becomes the working directory's name.
* Updated the .info file for each target.
* Added automated version release tool ([doc](development-workflow#markdown-header-version-release))
* Added stat file for each target and a usage file for each command execution.
* Pypette version is checked at each execution.
* Added sample summary to CLIP listing raw directory and size.
* Parameterized exome targets.
* Added genome configuration for Chinese hamster.

## 0.3.0 - *12/06/2020*
* Added graphics for scanpy in rnaseq pipeline.
* Added CLIP - Command Line Interface for sugar-manipulating samples, targets and pipeline commands.

## 0.2.0 - *25/05/2020*
* Now can use multiple paths for sequencing runs' fastq directories ([Project Configuration](project-config#markdown-header-mapping-available-samples)).

## 0.1.0 - *21/05/2020*
* Can merge fastq files between sequencing runs ([Inter-run Merging](targets#markdown-header-inter-run-merging) ).
* Can execute targets interactively ([Clusterless Execution](cluster-execution#markdown-header-clusterless-execution))
* Can limit number of jobs/cores per node ([Maximum Jobs](cluster-execution#markdown-header-maximum-jobs))
* Can give multiple targets per command ([Targets](targets))
* Tolerance to custom input fastq filenames ([Flexible Fastq Inputs](targets#markdown-header-flexible-fastq-inputs))

## X.Y.Z - *--/--/--*
* 
* Embarrassing Void
*

## 0.0.0 - *30/10/2017*
* And so it begun.
